import { PlusOutlined } from '@ant-design/icons';
import { Button, message, Input, Tag } from 'antd';
import React, { useState, useRef } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import ProDescriptions from '@ant-design/pro-descriptions';
import UpdateForm from './components/UpdateForm';
import { rule, addRule, updateRule, removeRule } from '@/services/ant-design-pro/api';
import { SharedUiTable } from '@cuong.lv2/table'
import { ModalForm } from '@cuong.lv2/modal-form'
const handleAdd = async (fields) => {
  const hide = message.loading('Loading...');

  try {
    await addRule({ ...fields });
    hide();
    message.success('Added successfully');
    return true;
  } catch (error) {
    hide();
    message.error('Adding failed, please try again!');
    return false;
  }
};


const handleUpdate = async (fields) => {
  const hide = message.loading('Configuring');

  try {
    await updateRule({
      name: fields.name,
      desc: fields.desc,
      key: fields.key,
    });
    hide();
    message.success('Configuration is successful');
    return true;
  } catch (error) {
    hide();
    message.error('Configuration failed, please try again!');
    return false;
  }
};


const handleRemove = async (selectedRows) => {
  const hide = message.loading('Loading...');
  if (!selectedRows) return true;

  try {
    await removeRule({
      key: selectedRows.map((row) => row.key),
    });
    hide();
    message.success('Deleted successfully and will refresh soon');
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

const TableList = () => {

  const [createModalVisible, handleModalVisible] = useState(false);

  const [updateModalVisible, handleUpdateModalVisible] = useState(false);
  const [showDetail, setShowDetail] = useState(false);
  const actionRef = useRef();
  const [currentRow, setCurrentRow] = useState();
  const [selectedRowsState, setSelectedRows] = useState([]);

  const intl = useIntl();
  const columns = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.updateForm.ruleName.nameLabel"
          defaultMessage="Rule name"
        />
      ),
      property: 'name',
      tip: 'The rule name is the unique key',
      type: 'link'
    },
    {
      title: <FormattedMessage id="pages.searchTable.titleDesc" defaultMessage="Description" />,
      property: 'desc',
      type: 'textarea',
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.titleCallNo"
          defaultMessage="Number of service calls"
        />
      ),
      property: 'callNo',
     type:'price'
    },
    {
      title: <FormattedMessage id="pages.searchTable.titleStatus" defaultMessage="Status" />,
      property: 'status',
      type: 'customize',
      customize:(col, data, key) => {
        switch (data) {
          case 0:{
            return (
              <Tag><FormattedMessage
              id="pages.searchTable.nameStatus.default"
              defaultMessage="Shut down"
            /></Tag>
            )
          }
          case 1:{
            return (
              <Tag color="blue"> <FormattedMessage id="pages.searchTable.nameStatus.running" defaultMessage="Running" /></Tag>
            )
          }
          case 2:{
            return (
              <Tag color="success"><FormattedMessage id="pages.searchTable.nameStatus.online" defaultMessage="Online" /></Tag>
            )
          }
          case 3:{
            return (
              <Tag color="error"> <FormattedMessage
              id="pages.searchTable.nameStatus.abnormal"
              defaultMessage="Abnormal"
            /></Tag>
            )
          }
        
          default:
            break;
        }
      },
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.titleUpdatedAt"
          defaultMessage="Last scheduled time"
        />
      ),
      property: 'updatedAt',
      type: 'datetime',
     
    },
    {
      title: <FormattedMessage id="pages.searchTable.titleOption" defaultMessage="Operating" />,
      property: 'option',
      type: 'view-edit-delete',
    },
  ];
  return (
    <PageContainer>
     <SharedUiTable 
      columns={columns}
      data={rule}
     />
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.createForm.newRule',
          defaultMessage: 'New rule',
        })}
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value);

          if (success) {
            handleModalVisible(false);

            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      />
     
    </PageContainer>
  );
};

export default TableList;
